export default class ReceiptPrinter {
  constructor (products) {
    this.products = [...products];
  }

  print (barcodes) {
    // TODO: Please implement the method
    // <-start-
    
    function toThrowError(string){
      return string;
    };

    let priceSum = 0;
    
    let productCount = {'BC001':0,'BC003':0,'BC005':0,
    'BC007':0,'BC009':0};
      
    for(let i=0;i<barcodes.length;i++){
      let canFind = false;
      for(let j=0;j<this.products.length;j++){
        if(barcodes[i]==this.products[j].barcode){
          priceSum+=this.products[j].price;
          productCount[barcodes[i]]++;
          canFind = true;
        }
        
      }
      if(!canFind)
        toThrowError('Unknown barcode.');
    }   

    let receipt = '';

    receipt+='==================== Receipt ====================\n';
    for(let i=0;i<5;i++){
      if( productCount[barcodes[i]]!=0){
        receipt+=this.products[i].name+'                     x'+
            productCount[barcodes[i]]+'        '+this.products[i].unit+
            '\n';
      }
    }
    receipt+='\n\n';
    receipt+='===================== Total =====================\n';
    
    priceSum = priceSum.toFixed(2);
   
    receipt+=priceSum;

    return receipt;

    // --end->
  }
}

